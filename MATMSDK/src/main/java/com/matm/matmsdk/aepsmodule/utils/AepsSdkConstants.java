package com.matm.matmsdk.aepsmodule.utils;


public class AepsSdkConstants {

    // public static final String BASE_URL = "https://35.244.1.37/";
    public static final String BASE_URL = "https://mobile.9fin.co.in/";
    //public static final String BASE_URL = "https://indusindaeps.iserveu.online/";
    //public static final String BASE_URL = "https://newapp.iserveu.online";


    public static final int REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE = 1003;
    public static final int REQUEST_FOR_ACTIVITY_CASH_DEPOSIT_CODE = 1001;
    public static final int REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE = 1002;
    public static final int BALANCE_RELOAD = 1004;
    public static final String IIN_KEY = "IIN_KEY";
    public static final String TRANSACTION_STATUS_KEY = "TRANSACTION_STATUS_KEY";
    public static final String MICRO_ATM_TRANSACTION_STATUS_KEY = "MICRO_ATM_TRANSACTION_STATUS_KEY";
    public static final String TRANSACTION_STATUS_KEY_SDK = "TRANSACTION_STATUS_KEY_SDK";
    public static final String DATEPICKER_TAG = "datepicker";
    public static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";
    public static final String USER_PINCODE = "USER_PINCODE";
    public static final String USER_LATLONG = "USER_LATLONG";
    public static final String USER_CITY = "USER_CITY";
    public static final String USER_STATE = "USER_STATE";
    public static final String NEXT_FRESHNESS_FACTOR = "NEXT_FRESHNESS_FACTOR";
    public static final String EASY_AEPS_PREF_KEY = "EASY_AEPS_PREF_KEY";
    public static final String EASY_AEPS_USER_LOGGED_IN_KEY = "EASY_AEPS_USER_LOGGED_IN_KEY";
    public static final String EASY_AEPS_USER_NAME_KEY = "EASY_AEPS_USER_NAME_KEY";
    public static final String encryptedString = "encryptedString";
    public static final String TEST_USER_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTI5OTkyMDQxNTE1LCJleHAiOjE1MzAwMjgwNDF9.Se0zZhaoemx6eeAVlE_9N_LqkhylDgsJS8f1-5Y4uIYIfFV_7Gst5JhwnA-ogjs4whk9x9VnNvBL8T-AVkctnQ";
    public static final String RETURN_URL = "http://10.15.20.131:8023/PG/TestPage.aspx";
    public static final String VERSION = "1000";
    public static final String SERVICE_AEPS_TS = "AEPStransactionStatus";
    public static final String SERVICE_AEPS_CW = "AEPScashWithdrawal";
    public static final String SERVICE_AEPS_BE = "AEPSbalanceEnquiry";
    public static final String SERVICE_MICRO_TS = "MATMtransactionStatus";
    public static final String SERVICE_MICRO_CW = "MATMcashWithdrawal";
    public static final String SERVICE_MICRO_BE = "MATMbalanceEnquiry";
    public static final String CLIENT_REQUEST_ENCRYPTION_KEY = "11e9903b-7cde-4738-96fe-c8295c8232de";
    public static final String CLIENT_HEADER_ENCRYPTION_KEY = "982b0d01-b262-4ece-a2a2-45be82212ba1";
    public static final String MERCHANT_ID = "8270629964";
    public static final String AUTHKEY = "333-444-555";
    public static final String CLIENTID = "8";
    public static final int RELOADREPORTS = 1005;
    public static final String USB_DEVICE = "USB_DEVICE";


    //Added these new constats
    public static String transactionAmount = "";
    public static String transactionType = "";
    public static String paramA = "";
    public static String paramB = "";
    public static String paramC = "";
    public static String encryptedData = "";
    public static String balanceEnquiry = "0";
    public static String cashWithdrawal = "1";
    public static String ministatement = "2";

    public static String transactionResponse = "";
    public static String loginID = "";
    public static String responseData = "responseData";
    public static String tokenFromCoreApp = "";
    public static String userNameFromCoreApp = "";
    public static String applicationType = "";
    public static boolean bioauth = false;
    public static String applicationUserName = "";

    public static int REQUEST_CODE = 55;
    public static boolean transactionStatus = true;

    public static String RECEIVE_DRIVER_DATA = "";
    public static String DRIVER_ACTIVITY = "";
    public static String MANUFACTURE_FLAG = "";

    //on July 4 by manas
    //layout for aeps
    public static int dashboardLayout = 0;
    public static int bankList = 0;
    public static int bankItem = 0;

    //layout for bio auth
    public static int bioAuthLayout = 0;

    //layout for aeps status
    public static int aepsStatusLayout = 0;

    //layout for statement
    public static int statementLayout = 0;
    public static int statementItem = 0;

    //enable Response
    public static boolean showTrans = true;

    //edit amount
    public static boolean editable = true;

}
