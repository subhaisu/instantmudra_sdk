package com.matm.matmsdk.aepsmodule.transactionstatus;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.matm.matmsdk.ChooseCard.ChooseCardActivity;
import com.matm.matmsdk.FileUtils;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.permission.PermissionsActivity;
import com.matm.matmsdk.permission.PermissionsChecker;
import com.paxsz.easylink.api.EasyLinkSdkManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import isumatm.androidsdk.equitas.R;

import static com.matm.matmsdk.permission.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.matm.matmsdk.permission.PermissionsChecker.REQUIRED_PERMISSION;

public class TransactionStatusNewActivity extends AppCompatActivity {

    private RelativeLayout ll_maiin;
    LinearLayout detailsLayout;
    private TextView statusMsgTxt, statusDescTxt;
    private ImageView status_icon;
    private TextView date_time, rref_num, aadhar_number, bank_name, card_amount, card_transaction_type, card_transaction_amount, txnID;
    public EasyLinkSdkManager manager;
    private Button backBtn,downloadBtn;
    ChooseCardActivity chooseCardActivity;
    LinearLayout ll13, ll12;
    String balance = "N/A";
    String amount = "N/A";
    String referenceNo = "N/A";
    String bankName = "N/A";
    String aadharCard = "N/A";
    String txnid = "N/A";
    TransactionStatusModel transactionStatusModel;
    PermissionsChecker checker;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SdkConstants.aepsStatusLayout == 0) {
            setContentView(R.layout.activity_transaction_status_aeps1);
        } else {
            setContentView(SdkConstants.aepsStatusLayout);
        }

        ll_maiin = findViewById(R.id.ll_maiin);
        ll13 = findViewById(R.id.ll13);
        ll12 = findViewById(R.id.ll12);
        detailsLayout = findViewById(R.id.detailsLayout);
        transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(SdkConstants.TRANSACTION_STATUS_KEY);
        //Runtime permission request required if Android permission >= Marshmallow
        checker = new PermissionsChecker(this);
        mContext = getApplicationContext();

        txnID = findViewById(R.id.txnID);
        card_transaction_amount = findViewById(R.id.card_transaction_amount);
        statusMsgTxt = findViewById(R.id.statusMsgTxt);
        status_icon = findViewById(R.id.status_icon);
        aadhar_number = findViewById(R.id.aadhar_number);
        rref_num = findViewById(R.id.rref_num);
        bank_name = findViewById(R.id.bank_name);
        card_transaction_type = findViewById(R.id.card_transaction_type);
        card_amount = findViewById(R.id.card_amount);
        statusDescTxt = findViewById(R.id.statusDescTxt);
        backBtn = findViewById(R.id.backBtn);
        downloadBtn = findViewById(R.id.downloadBtn);
        date_time = findViewById(R.id.date_time);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd : HH.mm.ss");
        String currentDateandTime = sdf.format(new Date());
        date_time.setText(currentDateandTime);
        if (getIntent().getSerializableExtra(SdkConstants.TRANSACTION_STATUS_KEY) == null) {
            ll_maiin.setBackgroundColor(getResources().getColor(R.color.statusfail));
            status_icon.setImageResource(R.drawable.ic_errorrr);
            statusMsgTxt.setText("Failed.");
            detailsLayout.setVisibility(View.GONE);
            backBtn.setBackgroundResource(R.drawable.button_backgroundtransaction_fail);

        } else {
            TransactionStatusModel transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(SdkConstants.TRANSACTION_STATUS_KEY);

            if (transactionStatusModel.getStatus().trim().equalsIgnoreCase("0")) {

                aadharCard = transactionStatusModel.getAadharCard();
                if (transactionStatusModel.getAadharCard() == null) {
                    aadharCard = "N/A";
                } else {
                    if (transactionStatusModel.getAadharCard().equalsIgnoreCase("")) {
                        aadharCard = "N/A";
                    } else {
                        StringBuffer buf = new StringBuffer(aadharCard);
                        buf.replace(0, 10, "XXXX-XXXX-");
                        System.out.println(buf.length());
                        aadharCard = buf.toString();
                    }
                }


                if (transactionStatusModel.getTxnID() != null && !transactionStatusModel.getTxnID().matches("")) {
                    txnid = transactionStatusModel.getTxnID();
                }
                if (transactionStatusModel.getBankName() != null && !transactionStatusModel.getBankName().matches("")) {
                    bankName = transactionStatusModel.getBankName();
                }
                if (transactionStatusModel.getReferenceNo() != null && !transactionStatusModel.getReferenceNo().matches("")) {
                    referenceNo = transactionStatusModel.getReferenceNo();
                }

                if (transactionStatusModel.getBalanceAmount() != null && !transactionStatusModel.getBalanceAmount().matches("")) {
                    balance = transactionStatusModel.getBalanceAmount();
                    if (balance.contains(":")) {
                        String[] separated = balance.split(":");
                        balance = separated[1].trim();
                    }
                }

                if (transactionStatusModel.getTransactionAmount() != null && !transactionStatusModel.getTransactionAmount().matches("")) {
                    amount = transactionStatusModel.getTransactionAmount();
                }


                if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Cash Withdrawal")) {
                    ll13.setVisibility(View.VISIBLE);
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                } else if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquery")) {

                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                }
            } else {
                String aadharCard = transactionStatusModel.getAadharCard();
                if (transactionStatusModel.getAadharCard() == null) {
                    aadharCard = "N/A";
                } else {
                    if (transactionStatusModel.getAadharCard().equalsIgnoreCase("")) {
                        aadharCard = "N/A";
                    } else {
                        StringBuffer buf = new StringBuffer(aadharCard);
                        buf.replace(0, 10, "XXXX-XXXX-");
                        System.out.println(buf.length());
                        aadharCard = buf.toString();
                    }
                }
                if (transactionStatusModel.getTxnID() != null && !transactionStatusModel.getTxnID().matches("")) {
                    txnid = transactionStatusModel.getTxnID();
                }
                if (transactionStatusModel.getBankName() != null && !transactionStatusModel.getBankName().matches("")) {
                    bankName = transactionStatusModel.getBankName();
                }

                if (transactionStatusModel.getReferenceNo() != null && !transactionStatusModel.getReferenceNo().matches("")) {
                    referenceNo = transactionStatusModel.getReferenceNo();
                }

                if (transactionStatusModel.getBalanceAmount() != null && !transactionStatusModel.getBalanceAmount().matches("")) {
                    balance = transactionStatusModel.getBalanceAmount();
                    if (balance.contains(":")) {
                        String[] separated = balance.split(":");
                        balance = separated[1].trim();
                    }
                }

                if (transactionStatusModel.getTransactionAmount() != null && !transactionStatusModel.getTransactionAmount().matches("")) {
                    amount = transactionStatusModel.getTransactionAmount();
                }
                ll_maiin.setBackgroundColor(Color.parseColor("#D94237"));
                status_icon.setImageResource(R.drawable.ic_errorrr);
                backBtn.setBackgroundResource(R.drawable.button_backgroundtransaction_fail);
                statusMsgTxt.setText("Failed");
                statusDescTxt.setText(transactionStatusModel.getApiComment());

                if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Cash Withdrawal")) {
                    ll13.setVisibility(View.VISIBLE);
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                } else if (transactionStatusModel.getTransactionType().equalsIgnoreCase("Balance Enquery")) {
                    txnID.setText(txnid);
                    rref_num.setText(referenceNo);
                    aadhar_number.setText(aadharCard);
                    bank_name.setText(bankName);
                    card_amount.setText(balance);
                    card_transaction_amount.setText(amount);
                    card_transaction_type.setText(transactionStatusModel.getTransactionType());

                }
            }
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        downloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
                    PermissionsActivity.startActivityForResult(TransactionStatusNewActivity.this, PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
                } else {


                    File dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    File imageFile = new File(dirPath, "Order_Receipt.pdf");

                    try {
                        if(!dirPath.isDirectory()) {
                            dirPath.mkdirs();
                        }
                        imageFile.createNewFile();

                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                   // createPdf(FileUtils.getAppPath(mContext) + "Order_Receipt.pdf");
                    createPdf(imageFile.toString());
                }
            }
        });
    }

    public void createPdf(String dest) {

        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
             * Creating Document
             */
            Document document = new Document();

            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(dest));

            // Open to write
            document.open();

            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("");
            document.addCreator("");

            /**
             * How to USE FONT....
             */
            BaseFont urName = BaseFont.createFont("assets/fonts/brandon_medium.otf", "UTF-8", BaseFont.EMBEDDED);

            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));

            BaseFont bf = BaseFont.createFont(
                    BaseFont.TIMES_ROMAN,
                    BaseFont.CP1252,
                    BaseFont.EMBEDDED);
            Font font = new Font(bf, 30);
            Font font2 = new Font(bf, 26);

            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell(new Paragraph("Transaction Receipt",font));
            cell.setColspan(2); // colspan
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            table.addCell(new Paragraph("Transaction ID",font)); // how to change cell to have different font and bold and background color
            table.addCell(new Paragraph(txnID.getText().toString(),font2)); // how to change cell to have different font and bold and background color
            table.addCell(new Paragraph("Aadhar Number",font));
            table.addCell(new Paragraph(aadhar_number.getText().toString(),font2));
            table.addCell(new Paragraph("Date/Time",font));
            table.addCell(new Paragraph(date_time.getText().toString(),font2));
            table.addCell(new Paragraph("Bank Name",font));
            table.addCell(new Paragraph(bank_name.getText().toString(),font2));
            table.addCell(new Paragraph("RREF NUM",font));
            table.addCell(new Paragraph(rref_num.getText().toString(),font2));
            table.addCell(new Paragraph("Balance Amount",font));
            table.addCell(new Paragraph(card_transaction_amount.getText().toString(),font2));
            table.addCell(new Paragraph("Transaction Type",font));
            table.addCell(new Paragraph(card_transaction_type.getText().toString(),font2));
            document.add(table);




            // Title Order Details...
            // Adding Title....
            Font mOrderDetailsTitleFont;
            if(statusMsgTxt.getText().toString().equalsIgnoreCase("FAILED")){
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.RED);

            }else{
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.GREEN);
            }

            Chunk mOrderDetailsTitleChunk = new Chunk(statusMsgTxt.getText().toString(), mOrderDetailsTitleFont);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsTitleParagraph);
            document.close();

            Toast.makeText(mContext, "PDF saved in the internal storage", Toast.LENGTH_SHORT).show();

            FileUtils.openFile(mContext, new File(dest));

        } catch (IOException | DocumentException ie) {
            Log.e("createPdf: Error ","" + ie.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == PermissionsActivity.PERMISSIONS_GRANTED) {
            Toast.makeText(mContext, "Permission Granted to Save", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Permission not granted, Try again!", Toast.LENGTH_SHORT).show();
        }
    }

}
