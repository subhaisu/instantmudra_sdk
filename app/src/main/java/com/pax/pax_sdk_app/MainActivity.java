package com.pax.pax_sdk_app;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.google.firebase.FirebaseApp;
import com.matm.matmsdk.Bluetooth.BluetoothActivity;
import com.matm.matmsdk.DemoActivity;
import com.matm.matmsdk.MPOS.PosActivity;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.AEPS2HomeActivity;
import com.matm.matmsdk.aepsmodule.AEPSHomeActivity;
import com.matm.matmsdk.aepsmodule.BioAuthActivity;
import com.matm.matmsdk.aepsmodule.CoreAEPSHomeActivity;
import com.matm.matmsdk.aepsmodule.LocationActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    RadioGroup rgTransactionType;
    RadioButton rbCashWithdrawal;
    RadioButton rbBalanceEnquiry,rb_mini;
    EditText etAmount,et_paramA,et_paramB;
    Button btnProceed,btn_pair,btn_proceedaeps,upiPagebtn,BtnUnpair,Btndriver;
    public static final int REQUEST_CODE = 5;
    ProgressDialog pd;
    Boolean isAepsClicked = false,isMatmClicked = false;
    String manufactureFlag = "";
    UsbManager musbManager;
    private UsbDevice usbDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        FirebaseApp.initializeApp(MainActivity.this);

        //retriveUserList();
        String str = "609384625620";
        String hashString = getSha256Hash(str);
        System.out.println("String Value :"+hashString);
    }



    private void initView() {
        musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
        rgTransactionType = findViewById(R.id.rg_trans_type);
        rbCashWithdrawal = findViewById(R.id.rb_cw);
        rbBalanceEnquiry = findViewById(R.id.rb_be);
        btnProceed = findViewById(R.id.btn_proceed);
        btn_pair = findViewById(R.id.btn_pair);
        rb_mini = findViewById(R.id.rb_mini);
        etAmount = findViewById(R.id.et_amount);
        btn_proceedaeps = findViewById(R.id.proceedBtn);
        upiPagebtn =findViewById(R.id.upiPagebtn);
        BtnUnpair = findViewById(R.id.BtnUnpair);
        Btndriver = findViewById(R.id.Btndriver);

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SdkConstants.BlueToothPairFlag.equalsIgnoreCase("1")){
                    if(PosActivity.isBlueToothConnected(MainActivity.this)){
                        callMATMSDKApp();
                    }else {
                        Toast.makeText(MainActivity.this, "Please pair the bluetooth device", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(MainActivity.this,"Please pair bluetooth device for ATM transaction.",Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_proceedaeps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(biometricDeviceConnect()){
                    callAEPSSDKApp();
                }else{
                    Toast.makeText(MainActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_pair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BluetoothActivity.class);
                intent.putExtra("userName","itpl");
                intent.putExtra("user_token","eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTk1MDY0MTM0NzczLCJleHAiOjE1OTUwNjU5MzR9.wJN0vX8pL7EIdfpuolckNb-wFVDz-AazzmCD5dutAlET8VneHMUfMwLKlBqSsFGcJUXFl0kCb0ygGKp_D7mb_A");

                SdkConstants.applicationType ="CORE";
                SdkConstants.tokenFromCoreApp =  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTk1MDY0MTM0NzczLCJleHAiOjE1OTUwNjU5MzR9.wJN0vX8pL7EIdfpuolckNb-wFVDz-AazzmCD5dutAlET8VneHMUfMwLKlBqSsFGcJUXFl0kCb0ygGKp_D7mb_A";
                SdkConstants.userNameFromCoreApp ="demoisu";




                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH,
                                    Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(),"Please Grant all the permissions", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(intent);
                        }
                    }else {
                        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH,
                                    Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                        } else {
                            startActivity(intent);
                        }
                    }
                }
                    else{
                    startActivity(intent);
                }
            }
        });
        BtnUnpair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SdkConstants.LogOut = "0";
                SdkConstants.BlueToothPairFlag = "0";
                Intent intent = new Intent(MainActivity.this,BluetoothActivity.class);
                intent.putExtra("user_id","488");
                intent.putExtra("user_token","eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTkzMjcwOTQzNzcwLCJleHAiOjE1OTMyNzI3NDN9.U6OHRnz-UvkcDOicmUYm_yT8FSDofbzZ9H8kJsLdLwgTRwt5vJ0PIwTKYC1JRpM3kXwqGg6sCpvQn1PmUz9lgw");

                startActivity(intent);
            }
        });

        Btndriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DemoActivity.class);
                intent.putExtra("activity","com.pax.pax_sdk_app.DriverActivity");
                intent.putExtra("driverFlag","MANTRA");

                startActivity(intent);
            }
        });

        upiPagebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UPIMainActivity.class);
                startActivity(intent);
            }
        });

        rgTransactionType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_cw) {
                    etAmount.setClickable(true);
                    etAmount.setHint("Amount");
                    etAmount.setVisibility(View.VISIBLE);
                    etAmount.setText("");
                    etAmount.setEnabled(true);
                    etAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
                else if (checkedId == R.id.rb_be) {
                    etAmount.setVisibility(View.GONE);
                    etAmount.setClickable(false);
                    etAmount.setEnabled(false);
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null & resultCode == RESULT_OK) {

            /**
             *  FOR AEPS TRANSACTION RESPONSE
             */
            if (requestCode == SdkConstants.REQUEST_CODE) {
                String response = data.getStringExtra(SdkConstants.responseData);
                System.out.println("Response: "+response);
                //Toast.makeText(MainActivity.this,response,Toast.LENGTH_SHORT).show();
            }
            /**
             *  FOR MATM TRANSACTION RESPONSE
             */
            if (requestCode == SdkConstants.MATM_REQUEST_CODE) {
                String response = data.getStringExtra(SdkConstants.responseData);
                System.out.println("Response: "+response);
                //Toast.makeText(MainActivity.this,response,Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void callAEPSSDKApp() {
        SdkConstants.transactionAmount = etAmount.getText().toString().trim();
        if(rbCashWithdrawal.isChecked()){
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;

        }
        else if(rbBalanceEnquiry.isChecked()){
            SdkConstants.transactionType= SdkConstants.balanceEnquiry;
        }
        else if(rb_mini.isChecked()){
            SdkConstants.transactionType= SdkConstants.ministatement;
        }

        SdkConstants.paramA = "annapurna";
        SdkConstants.paramB = "BLS1";
        SdkConstants.paramC = "loanID";

//        SdkConstants.applicationType ="CORE";
//        SdkConstants.tokenFromCoreApp =  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTk3ODIxMzE2ODU3LCJleHAiOjE1OTc4MjMxMTZ9.V1IS8nNDTwQ-yALKQ5Gjk3K_kwC3OpSNZ2N2Jr93eE1Bj8P5XitOMn1PLujtfByFxNJWkq2EmTYvrMDk6mlKvA";
//        SdkConstants.userNameFromCoreApp = "itpl";



        //annarpurna
//        SdkConstants.loginID = "aepsTestR";
//        SdkConstants.encryptedData ="cssC%2BcHGxugRFLTjpk%2BJN2Hbbo%2F%2BDokPsBwb9uFdXebdGg%2FEaqOvFXBEoU7ve%2FAP6rabeaskLloqjx6bF6tCcw%3D%3D";
       //instantmudra
        SdkConstants.loginID = "7978628756";
        SdkConstants.encryptedData ="TYqmJRyB%2B4Mb39MQf%2BPqVpG%2BMXYkFjv7FvFq5zSop426IBfOKVTFtcsgZDUCORAu%2FDJvr85SGAUeQVWgRINTI5teZqYzUL1nyFMcf1eO69A%3D";


        System.out.println("Rajesh::::"+manufactureFlag);
        SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
        SdkConstants.DRIVER_ACTIVITY = "com.pax.pax_sdk_app.DriverActivity";

        Intent intent = new Intent(MainActivity.this, AEPS2HomeActivity.class);
        //intent.putExtra("activity","com.pax.pax_sdk_app.DriverActivity");
        //intent.putExtra("driverFlag",manufactureFlag);
        startActivity(intent);


    }

    private void callMATMSDKApp() {
        SdkConstants.transactionAmount = etAmount.getText().toString().trim();
        if(rbCashWithdrawal.isChecked()){
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
            SdkConstants.transactionAmount = etAmount.getText().toString();

        }if(rbBalanceEnquiry.isChecked()){
            SdkConstants.transactionType= SdkConstants.balanceEnquiry;
            SdkConstants.transactionAmount = "0";
        }
        SdkConstants.paramA = "123456789";
        SdkConstants.paramB = "branch1";
        SdkConstants.paramC = "loanID1234";
        //SdkConstants.loginID = "aepsTestR";

//        SdkConstants.applicationType ="CORE";
//        SdkConstants.tokenFromCoreApp =  "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTk0OTgxNjU0MDM3LCJleHAiOjE1OTQ5ODM0NTR9.2z7GR-vn0fubE_tgheLDYEb_YG9Faqgc8u1NNFxzCCCuF_12F81WH76AImLZ9aJesqaQCOk9jhp6zXnUAPKI2A";
//        SdkConstants.userNameFromCoreApp = "itpl";

//        SdkConstants.loginID = "aepsTestR";
//        SdkConstants.encryptedData ="cssC%2BcHGxugRFLTjpk%2BJN2Hbbo%2F%2BDokPsBwb9uFdXebdGg%2FEaqOvFXBEoU7ve%2FAP6rabeaskLloqjx6bF6tCcw%3D%3D";



        Intent intent = new Intent(MainActivity.this, LocationActivity.class);
        startActivityForResult(intent, SdkConstants.MATM_REQUEST_CODE);
    }

    @Override
    protected void onResume() {
        String str = SdkConstants.responseData;
        Toast.makeText(getApplicationContext(),str,Toast.LENGTH_LONG).show();
        super.onResume();
    }


    public  String getSha256Hash(String password) {
        try {
            MessageDigest digest = null;
            try {
                digest = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e1) {
                e1.printStackTrace();
            }
            digest.reset();
            return bin2hex(digest.digest(password.getBytes()));
        } catch (Exception ignored) {
            return null;
        }
    }

    private  String bin2hex(byte[] data) {
        StringBuilder hex = new StringBuilder(data.length * 2);
        for (byte b : data)
            hex.append(String.format("%02x", b & 0xFF));
        return hex.toString();
    }

    private Boolean biometricDeviceConnect(){
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            deviceConnectMessgae();
            return false;
        }
        else {
            for (UsbDevice device : connectedDevices.values()) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if(device !=null && device.getManufacturerName () != null){
                        usbDevice = device;
                        manufactureFlag = usbDevice.getManufacturerName();
                        return true;
                    }

                }
            }
        }
        return false;
    }

    private void deviceConnectMessgae (){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MainActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle("No device found")
                .setMessage("Unable to find biometric device connected . Please connect your biometric device for AEPS transactions.")
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                       // finish();
                    }
                })
                .show();
    }
}
